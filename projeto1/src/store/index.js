import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import axios from "axios";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "chapter-front",
  storage: window.sessionStorage,
});

export default new Vuex.Store({
  state: {
    boxer: {},
    stacks: {},
  },
  getters: {
    boxerDetail({ boxer }) {
      return boxer;
    },
    stacks({ stacks }) {
      return stacks;
    },
  },
  mutations: {
    setBoxer(state, payload) {
      state.boxer = payload;
    },
    setStacks(state, payload) {
      state.stacks = payload;
    },
  },
  plugins: [vuexPersist.plugin],
  actions: {
    async getStacks(context) {
      await axios
        .get("http://demo6105689.mockable.io/stacks")
        .then(({ data }) => {
          context.commit("setStacks", data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
  },
});
