import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Detalhes from "../views/Detalhes.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    name: "Home",
    component: Home,
  },
  {
    path: "/details",
    name: "detalhes",
    component: Detalhes,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
